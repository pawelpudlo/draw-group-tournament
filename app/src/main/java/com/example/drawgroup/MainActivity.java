package com.example.drawgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    List<String> teamList = new ArrayList<>();
    List<String> groupA = new ArrayList<>();
    List<String> groupB = new ArrayList<>();

    private String names = "";

    TextView team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refresh();
    }

    public void addTeam(View view) {

        EditText team = findViewById(R.id.editText);

        String name = "" + team.getText();

        this.teamList.add(name);

    }

    private void refreshRun(int milisecound) {

        final Handler handler = new Handler();

        final Runnable run = new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        };
        handler.postDelayed(run, milisecound);
    }

    public void refresh() {

        team = findViewById(R.id.teams);

        names = "";
        for (int i = 0; i < teamList.size(); i++) {
            names += i + 1 + "." + teamList.get(i) + "\n";
        }

        team.setText(names);

        refreshRun(1000);
    }

    public void drawGroup(View view) {

        Intent intent = new Intent(this, EndActivity.class);
        int sizeTeamList = this.teamList.size();
        Collections.sort(this.teamList);


        if(sizeTeamList < 4){
            team = findViewById(R.id.teams);
            team.setText("Minimum 4 drużyny");
        }else{

            int groupSize = sizeTeamList/2;
            int modulo = sizeTeamList % 2;

            Random generator = new Random();
            int group;


            String key;
            String name;

            for(int i=0; i<this.teamList.size(); i++) {

                group = generator.nextInt(2);

                if(this.groupA.size() == groupSize && this.groupB.size() == groupSize && modulo !=0){
                    this.groupA.add(this.teamList.get(i));
                }else if(this.groupB.size() == groupSize){
                    this.groupA.add(this.teamList.get(i));
                }else if(this.groupA.size() == groupSize){
                    this.groupB.add(this.teamList.get(i));
                }else{
                    if(group == 0){
                        this.groupA.add(this.teamList.get(i));
                    }else{
                        this.groupB.add(this.teamList.get(i));
                    }
                }

            }
            String groupsStr = writeGroups(sizeTeamList);
            saveData("groups", groupsStr);
            startActivity(intent);
        }
    }

    private String writeGroups(int size){

        String groups = " \n Grupa A \n";
        for(int i=0; i<this.groupA.size(); i++){
            groups = groups + this.groupA.get(i) + "\n";
        }


        groups = groups + " \n Grupa B \n";
        for(int i=0; i<this.groupB.size(); i++){
            groups = groups + this.groupB.get(i) + "\n";
        }

        return groups;
    }

    public void saveData(String key,String output) {
        SharedPreferences sharedPreferences = getSharedPreferences("TEST", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, output);
        editor.apply();
    }

    public void back(View view) {
        if(teamList.size()!=0){
            teamList.remove(teamList.size()-1);
        }
    }
}