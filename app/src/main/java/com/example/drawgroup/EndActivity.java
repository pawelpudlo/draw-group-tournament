package com.example.drawgroup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class EndActivity extends AppCompatActivity {

    TextView groups;
    TextView endTime;

    private int time = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);
        groups = findViewById(R.id.groups);

        endTime = findViewById(R.id.licznik);
        loadData();
        refresh();
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("TEST", MODE_PRIVATE);
        String first = sharedPreferences.getString("groups", "");

        groups.setText(first);
    }

    private void refreshRun(int milisecound) {

        final Handler handler = new Handler();

        final Runnable run = new Runnable() {
            @Override
            public void run() {
                refresh();
            }
        };
        handler.postDelayed(run, milisecound);
    }

    public void refresh() {
        endTime.setText(time+"");
        if(time==-1){
            groups.setVisibility(View.VISIBLE);
            endTime.setVisibility(View.INVISIBLE);
        }else{
            time--;
        }
        refreshRun(1000);
    }
}